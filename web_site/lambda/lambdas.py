import json
import traceback
import logging
import requests


def _return(code, body):
    """
    private method that returns status response as json object
    it provides a headers element for CORs compliance
    an http status code
    and body content.
    """
    return {
        "headers": {
            'Access-Control-Allow-Origin': '*'
        },
        "statusCode": code,
        "body": body
    }


def _configure_logging(level):
    """
    private method that configures logging to requested level
    returns log instance
    """

    log = logging.getLogger()
    log.setLevel(level)
    return log


def _sanity_check_inputs(event):
    """
    private method that makes sure queryStringParameters dictionary is present
    returns a 200 status if queryStringParameters dictionary is present.
    returns 400 status if event is None, or queryStringParameters dictionary is missing
    """

    if event is None:
        return _return(400, "Missing event")

    query_params = event['queryStringParameters']

    if query_params is None:
        dumped = json.dumps(event)
        return _return(400, "Missing 'queryStringParameters' in 'event " + dumped)

    return _return(200, "event OK")


def _handle_500_error(log, event):
    log.error(traceback.format_exc())
    if event is not None:
        details = json.dumps(event)
    else:
        details = "event it NoneType"
    return _return(500, "Error executing lambda " + traceback.format_exc() + " --> " + details)


def get_location(event, context):
    """ Looks for ip in event (passed as query parameter) and using that IP returns either
        400 (no location found for ip)
        200 in which case a location dictionary with the keys latitude and longitude
        in the body element of the response is returned
        500 is the call to get the location fails due to either an invalid request, or error
        from the service.
    """
    log = _configure_logging(logging.INFO)
    try:
        check = _sanity_check_inputs(event)
        if check['statusCode'] is not 200:
            return check

        query_params = event['queryStringParameters']
        if not 'ip' in query_params:
            _return(400, "Missing 'ip' query parameter")

        ip_addr = query_params['ip']
        api_url = f"https://ipvigilante.com/{ip_addr}"

        data = requests.get(api_url)
        if data.status_code == 400:
            return _return(400, "Invalid Request to: " + api_url + " %s" % data.text)

        elif data.status_code == 500:
            return _return(500, "Error from" + api_url + "s" + " %s" % data.text)

        response_dict = data.json()
        location = {
            "latitude": response_dict["data"]["latitude"],
            "longitude": response_dict["data"]["longitude"],
        }

        return _return(200, json.dumps(location))

    except:
        log.error(traceback.format_exc())
        return _return(500, "Error executing lambda: " + traceback.format_exc())


def get_weather(event, context):
    """ given latitude and longitude as query parameters, return the weather for
        that location.
        returns
        400 Invalid Request to dark sky weather API
        500 Error from dark sky sky weather API
        200 and json with weather like this:
        {
          "summary": "Clear",
          "temperature": 51.77,
          "humidity": 0.59,
          "apparentTemperature": 51.77,
          "high": 71.08,
          "low": 46.36}
        }
    """
    log = _configure_logging(logging.INFO)

    DARK_SKY_SECRET_KEY = "fbcf15d3d872f2f66720690d60e26343"

    try:
        check = _sanity_check_inputs(event)
        if check['statusCode'] is not 200:
            return check

        query_params = event['queryStringParameters']
        if not query_params['latitude']:
            _return(400, "latitude not in query_params")
        if not query_params['longitude']:
            _return(400, "longitude not in query_params")

        latitude = query_params['latitude']
        longitude = query_params['longitude']

        api_url = f"https://api.darksky.net/forecast/{DARK_SKY_SECRET_KEY}/{latitude},{longitude}"
        data = requests.get(api_url)

        log.info("Recieved HTTP %s from Dark Sky" % data.status_code)

        if data.status_code == 400:
            return _return(400, "Invalid Request to dark sky: %s" % data.text)

        elif data.status_code == 500:
            return _return(500, "Error from dark sky: %s" % data.text)

        response_dict = data.json()

        current_forecast = response_dict.get("currently")
        today_daily_forecast = response_dict.get("daily").get("data")[0]

        forecast = {
            "summary": current_forecast.get("summary"),
            "temperature": current_forecast.get("temperature"),
            "humidity": current_forecast.get("humidity"),
            "apparentTemperature": current_forecast.get("apparentTemperature"),
            "high": today_daily_forecast.get("temperatureHigh"),
            "low": today_daily_forecast.get("temperatureLow")
        }
        return _return(200, json.dumps(forecast))

    except:
        _handle_500_error(log, event)
