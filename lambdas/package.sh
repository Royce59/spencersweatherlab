#!/usr/bin/env bash
rm function.zip
cd package/
zip -r9 ${OLDPWD}/functions.zip .
cd ..
zip -g functions.zip lambdas.py
echo "done creating zip"