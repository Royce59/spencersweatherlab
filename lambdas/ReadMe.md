## Packaging instructions 

If this is a clean checkout execute: <P>

<code>
pip install --target ./package Requests
</code>

That will create a package directory with the Request dependency in it.

To create a zip file to upload to AWS Lambda type

package.sh

It will create a file called <code>functions.zip</code> in the root directory of the project
Via the console you can upload this file to AWS Lambda 
  