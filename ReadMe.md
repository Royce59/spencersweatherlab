# session3weatherlab
### smarks@origamisoftware.com 

copyright Spencer Marks 
use granted under http://www.apache.org/licenses/LICENSE-2.0

## lambdas
Python code for weather service endpoints. Includes tests and an additional ReadMe.md

 
def get_location(event, context):
 
   		Looks for ip in event (passed as query parameter) and using that IP returns either
        400 (no location found for ip)
        200 in which case a location dictionary with the keys latitude and longitude
        in the body element of the response is returned
        500 is the call to get the location fails due to either an invalid request, or error
        from the service.
  
 
def get_weather(event, context):
 
  		Given latitude and longitude as query parameters, return the weather for
        that location.
        returns
        400 Invalid Request to dark sky weather API
        500 Error from dark sky sky weather API
        200 and json with weather like this:
        {
          "summary": "Clear",
          "temperature": 51.77,
          "humidity": 0.59,
          "apparentTemperature": 51.77,
          "high": 71.08,
          "low": 46.36}
        }
    

## web_site
A simple, static, website the makes AJAX calls to Lambdas described above. 
